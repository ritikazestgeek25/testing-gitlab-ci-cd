#!/bin/bash

# get GitLab access token
export GITLAB_ACCESS_TOKEN=$GITLAB_ACCESS_TOKEN

# Echo the access token for verification
echo "GITLAB_ACCESS_TOKEN: $GITLAB_ACCESS_TOKEN"

# Create .git-credentials file with GitLab access token
echo "https://oauth2:$GITLAB_ACCESS_TOKEN@gitlab.com" > .git-credentials

# Configure Git to use .git-credentials for authentication
git config --global credential.helper "store --file=.git-credentials"