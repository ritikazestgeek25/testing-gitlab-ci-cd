#!/bin/bash

# Define variables
PROJECT_ID="$CI_PROJECT_ID"
GITALB_API_TOKEN=$GITLAB_ACCESS_TOKEN_TO_MUTATE
TAG_NAME=$CI_COMMIT_TAG

# Define variables
url="https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests?state=merged&target_branch=main"

# Send the request and parse JSON response
response=$(curl -s --header "PRIVATE-TOKEN: $GITALB_API_TOKEN" "$url")

# Extract title and description of the first object
title=$(echo "$response" | jq -r '.[0].title')
description=$(echo "$response" | jq -r '.[0].description')

# Export the variables
export title
export description

# Print the extracted fields
echo "Title: $title"
echo "Description: $description"


echo "TITLE=$title" >> variables.env


echo "DESCRIPTION="$description"" >> variables.env

username="project_${PROJECT_ID}_bot_$(openssl rand -hex 4)"
email="project_${project_id}_bot_$(openssl rand -hex 4)@noreply.gitlab.com"



git config user.email "$email"
git config user.name "$username"
echo "$email"
echo "$username"
git remote remove origin 
git remote add origin https://oauth2:$GITALB_API_TOKEN@gitlab.com/ritikazestgeek25/testing-gitlab-ci-cd.git

git tag -f -a "$TAG_NAME" -m "$description"
git push origin "$TAG_NAME" --force
echo "$(git remote -v)"



