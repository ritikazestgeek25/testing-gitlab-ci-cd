#!/bin/bash

# Variables
PROJECT_ID="$CI_PROJECT_ID"
MERGE_REQUEST_IID="$CI_MERGE_REQUEST_IID"
UPDATED_DESCRIPTION="Your updated description here"
GITALB_API_TOKEN=$GITLAB_ACCESS_TOKEN_TO_MUTATE

# Update merge request to enable squash
MERGE_REQUEST_DESCRIPTION_RESPONSE=$(curl --request PUT \
     --header "PRIVATE-TOKEN: $GITALB_API_TOKEN" \
     --header "Content-Type: application/json" \
     --data "{\"squash\": true}" \
     "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests/$MERGE_REQUEST_IID" 2>&1)

# Echo response
echo "$MERGE_REQUEST_DESCRIPTION_RESPONSE"