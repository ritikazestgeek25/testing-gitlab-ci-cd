#!/bin/bash

# VARIABLES
ACCESS_TOKEN="$1"
BRANCH="$3"
TAG_NAME="$4"
TAG_MESSAGE="$5"

# Variables
PROJECT_ID="$CI_PROJECT_ID"
MERGE_REQUEST_IID="$CI_MERGE_REQUEST_IID"
UPDATED_DESCRIPTION="Your updated description here"
GITALB_API_TOKEN=$GITLAB_ACCESS_TOKEN_TO_MUTATE

# Sends a curl POST request to Gitlab to create a tag commit
curl --request POST \
  --header "PRIVATE-TOKEN: $ACCESS_TOKEN" \
  --url "https://gitlab.covergenius.biz/api/v4/projects/$PROJECT_ID/repository/tags?tag_name=$TAG_NAME&ref=$BRANCH&message=$TAG_MESSAGE"
