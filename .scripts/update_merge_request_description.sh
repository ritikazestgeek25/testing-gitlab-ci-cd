#!/bin/bash

# Define variables
PROJECT_ID="$CI_PROJECT_ID"
MERGE_REQUEST_IID="$CI_MERGE_REQUEST_IID"
UPDATED_DESCRIPTION="Your updated description here"
GITALB_API_TOKEN=$GITLAB_ACCESS_TOKEN_TO_MUTATE

echo $PROJECT_ID
echo $MERGE_REQUEST_IID
echo "gitlab api token"
echo $GITALB_API_TOKEN
echo CI_GITALB_API_TOKEN
echo $GITLAB_ACCESS_TOKEN_TO_MUTATE
echo "gitlab api token"
echo $GITALB_API_TOKEN

# Get merge request details
MR_COMMITS=$(curl --header "PRIVATE-TOKEN: $GITALB_API_TOKEN" "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests/$MERGE_REQUEST_IID/commits")


echo "MR commits message"
echo $MR_COMMITS
# mr commits
# Extract commit names
# Initialize empty string

# Loop over JSON data and extract titles
for commit in $(echo "$MR_COMMITS" | jq -r '.[] | .title'); do
    UPDATED_DESCRIPTION+="$commit "
done

# Print formatted titles
echo -e "$UPDATED_DESCRIPTION"


# Update merge request description
MERGE_REQUEST_DESCRIPTION_RESPONSE=$(curl --request PUT \
     --header "PRIVATE-TOKEN: $GITALB_API_TOKEN" \
     --header "Content-Type: application/json" \
     --data "{\"description\": \"$UPDATED_DESCRIPTION\"}" \
     "https://gitlab.com/api/v4/projects/$PROJECT_ID/merge_requests/$MERGE_REQUEST_IID" 2>&1)

# Echo response
echo "$MERGE_REQUEST_DESCRIPTION_RESPONSE"
